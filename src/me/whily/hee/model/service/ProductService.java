/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.whily.hee.model.service;

import java.sql.SQLException;
import java.util.List;
import javax.persistence.Query;
import me.whily.hee.model.entity.Product;
import me.whily.hee.model.entity.Protype;
import org.apache.log4j.Logger;

/**
 * product service
 * @author wanghy
 * @since 2015-05-23 14:46:42
 */
public class ProductService extends AbstractService {
    Logger logger = Logger.getLogger(this.getClass());
    /**
     * save product
     * @param pro 
     */
    public void save(Product pro) {
        try {
            beginTransaction(em);
            em.persist(pro);
            commitTransaction(em);
        } catch (SQLException ex) {
            logger.error("save product", ex);
        }
    }
    /**
     * update product
     * @param pro 
     */
    public void update(Product pro) {
        try {
            beginTransaction(em);
            em.merge(pro);
            commitTransaction(em);
        } catch (SQLException ex) {
            logger.error("update product", ex);
        }
    }
    
    /**
     * find product by type
     * @param typeid
     * @return
     */
    public List<Product> findProductByType(Integer typeid) {
        Query query = em.createNamedQuery("Product.findByPtid");
        query.setParameter("ptid", typeid);
        List list = query.getResultList();
        return list;
    }
    
    /**
     * save product type
     * @param type 
     */
    public void save(Protype type) {
        try {
            beginTransaction(em);
            int maxId = findMaxId();
            int maxSort = findMaxSortByPid(type.getPtpid());
            type.setPtid(maxId + 1);
            type.setPtsort(maxSort + 1);
            em.persist(type);
            commitTransaction(em);
        } catch (SQLException ex) {
            logger.error("save product type", ex);
            try {
                rollbackTransacrion(em);
            } catch (SQLException ex1) {
                logger.error("rollback transacrion", ex1);
            }
        }
    }
    
    /**
     * find protype max id
     * @return
     */
    public int findMaxId() {
        Query query = em.createNamedQuery("Protype.findMaxId");
        Object maxId = query.getSingleResult();
        if (maxId != null)
            return (Integer)maxId;
        return 1;
    }
    
    /**
     * find protype max sort by pid
     * @param pid
     * @return
     */
    public int findMaxSortByPid(int pid) {
        Query query = em.createNamedQuery("Protype.findMaxSortByPid");
        query.setParameter("pid", pid);
        Object sort = query.getSingleResult();
        if (sort != null)
            return (Integer)sort;
        return 1;
    }
    
    /**
     * find all protype
     * @return
     */
    public List<Protype> findProtype() {
        Query query = em.createNamedQuery("Protype.findAll");
        List list = query.getResultList();
        return list;
    }
    
    public static void main(String[] args) throws SQLException {
        ProductService service = new ProductService();
        Protype type = new Protype();
        type.setPtname("无衣");
        type.setPtpid(1);
//        service.save(type);
//        List<Product> pros = service.findProductByType(1);
        List<Protype> list = service.findProtype();
        for (Protype pt : list) {
            System.out.println("pt: " + pt.getPtname());
        }
    }
}
