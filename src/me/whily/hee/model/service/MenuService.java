/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.whily.hee.model.service;

import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import me.whily.hee.model.entity.Menu;
import me.whily.hee.model.entity.User;

/**
 * 
 * @author wanghy
 * @since 2015-05-23 14:46:42
 */
public class MenuService extends AbstractService {
    
    public void save(User user) throws SQLException {
        beginTransaction(em);
        em.persist(user);
        commitTransaction(em);
    }
    
    public List<Menu> findByUserId(String userId) throws SQLException {
        String sql = "select m.* from hee_menu m left join hee_user_menu um on m.mid = um.mid where um.uid=? order by m.mpid, m.msort";
        Query query = em.createNativeQuery(sql, Menu.class);
        query.setParameter(1, userId);
        return query.getResultList();
    }
    
    public static void main(String[] args) throws SQLException {
        MenuService service = new MenuService();
        
        List<Menu> menus = service.findByUserId("wanghy");
        for (Menu menu : menus) {
            System.out.println("menus: " + menu.getMname());
        }
        
    }
}
