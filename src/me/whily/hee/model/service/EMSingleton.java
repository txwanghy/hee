/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.whily.hee.model.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author wanghy
 * @since 2015-5-22 15:54:16
 */
public class EMSingleton {

    private EMSingleton(){}
    private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("heePU");
    private static final EntityManager em = factory.createEntityManager();
    public static EntityManager getInstance() {
        return em;
    }
}
