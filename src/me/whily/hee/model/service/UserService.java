/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.whily.hee.model.service;

import javax.persistence.EntityTransaction;
import me.whily.hee.model.entity.User;

/**
 * 
 * @author wanghy
 * @since 2015-05-23 14:46:42
 */
public class UserService extends AbstractService {
    
    public void save(User user) {
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(user);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public User findById(String id) {
        return em.find(User.class, id);
    }
    
    public static void main(String[] args) {
        UserService service = new UserService();
        
        User user = new User();
        user.setUid("wangll");
        user.setUname("王丽丽");
        user.setUpassword("45E6571459DA3CBC8723BC0858636589");
        service.save(user);
        System.out.println("succeed");
        
        User user1 = service.findById("wanghy");
        System.out.println("UserName: " + user1.getUname());
    }
}
