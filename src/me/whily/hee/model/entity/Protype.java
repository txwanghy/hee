/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.whily.hee.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author wanghy
 * @since 2015-6-3 11:08:33
 */
@Entity
@Table(name = "HEE_PROTYPE")
@NamedQueries({
    @NamedQuery(name = "Protype.findAll", query = "SELECT p FROM Protype p ORDER BY p.ptpid, p.ptsort"),
    @NamedQuery(name = "Protype.findMaxId", query = "SELECT max(p.ptid) FROM Protype p"),
    @NamedQuery(name = "Protype.findMaxSortByPid", query = "SELECT max(p.ptsort) FROM Protype p WHERE p.ptpid=:pid"),
})
public class Protype implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String ROOT_PID = "0";
    @Id
    @Basic(optional = false)
    @Column(name = "PTID")
    private Integer ptid;
    @Column(name = "PTPID")
    private Integer ptpid;
    @Column(name = "PTNAME")
    private String ptname;
    @Column(name = "PTSORT")
    private Integer ptsort;

    public Protype() {
    }

    public Protype(Integer ptid) {
        this.ptid = ptid;
    }

    public Integer getPtid() {
        return ptid;
    }

    public void setPtid(Integer ptid) {
        this.ptid = ptid;
    }

    public Integer getPtpid() {
        return ptpid;
    }

    public void setPtpid(Integer ptpid) {
        this.ptpid = ptpid;
    }

    public String getPtname() {
        return ptname;
    }

    public void setPtname(String ptname) {
        this.ptname = ptname;
    }

    public Integer getPtsort() {
        return ptsort;
    }

    public void setPtsort(Integer ptsort) {
        this.ptsort = ptsort;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptid != null ? ptid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Protype)) {
            return false;
        }
        Protype other = (Protype) object;
        if ((this.ptid == null && other.ptid != null) || (this.ptid != null && !this.ptid.equals(other.ptid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "me.whily.hee.model.entity.Protype[ ptid=" + ptid + " ]";
    }

}
