/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.whily.hee.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author wanghy
 * @since 2015-6-3 11:08:33
 */
@Entity
@Table(name = "HEE_PRODUCT")
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p"),
    @NamedQuery(name = "Product.findByPtid", query = "SELECT p FROM Product p WHERE p.ptid=:ptid"),
})
public class Product implements Serializable {
    @Column(name = "PTID")
    private Integer ptid;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROID")
    private String proid;
    @Column(name = "PRONAME")
    private String proname;
    @Column(name = "PROBARCODE")
    private String probarcode;
    @Column(name = "PROSPEC")
    private String prospec;
    @Column(name = "PROSRRC")
    private String prosrrc;
    @Column(name = "PROORIGIN")
    private String proorigin;
    @Column(name = "PROCOMPANY")
    private String procompany;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRORETAIL")
    private BigDecimal proretail;
    @Column(name = "PROMINPRICE")
    private BigDecimal prominprice;
    @Column(name = "PROPREPRICE1")
    private BigDecimal propreprice1;
    @Column(name = "PROPREPRICE2")
    private BigDecimal propreprice2;
    @Column(name = "PROREMARK")
    private BigDecimal proremark;

    public Product() {
    }

    public Product(String proid) {
        this.proid = proid;
    }

    public String getProid() {
        return proid;
    }

    public void setProid(String proid) {
        this.proid = proid;
    }

    public String getProname() {
        return proname;
    }

    public void setProname(String proname) {
        this.proname = proname;
    }

    public String getProbarcode() {
        return probarcode;
    }

    public void setProbarcode(String probarcode) {
        this.probarcode = probarcode;
    }

    public String getProspec() {
        return prospec;
    }

    public void setProspec(String prospec) {
        this.prospec = prospec;
    }

    public String getProsrrc() {
        return prosrrc;
    }

    public void setProsrrc(String prosrrc) {
        this.prosrrc = prosrrc;
    }

    public String getProorigin() {
        return proorigin;
    }

    public void setProorigin(String proorigin) {
        this.proorigin = proorigin;
    }

    public String getProcompany() {
        return procompany;
    }

    public void setProcompany(String procompany) {
        this.procompany = procompany;
    }

    public BigDecimal getProretail() {
        return proretail;
    }

    public void setProretail(BigDecimal proretail) {
        this.proretail = proretail;
    }

    public BigDecimal getProminprice() {
        return prominprice;
    }

    public void setProminprice(BigDecimal prominprice) {
        this.prominprice = prominprice;
    }

    public BigDecimal getPropreprice1() {
        return propreprice1;
    }

    public void setPropreprice1(BigDecimal propreprice1) {
        this.propreprice1 = propreprice1;
    }

    public BigDecimal getPropreprice2() {
        return propreprice2;
    }

    public void setPropreprice2(BigDecimal propreprice2) {
        this.propreprice2 = propreprice2;
    }

    public BigDecimal getProremark() {
        return proremark;
    }

    public void setProremark(BigDecimal proremark) {
        this.proremark = proremark;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proid != null ? proid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.proid == null && other.proid != null) || (this.proid != null && !this.proid.equals(other.proid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "me.whily.hee.model.entity.Product[ proid=" + proid + " ]";
    }

    public Integer getPtid() {
        return ptid;
    }

    public void setPtid(Integer ptid) {
        this.ptid = ptid;
    }

}
