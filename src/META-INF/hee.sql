use database HEE;
create table HEE_USER (
    UID varchar(32),
    UNAME varchar(32),
    UPASSWORD varchar(32),
    primary key (UID)
);
/*
comment on table HEE_USER is '用户表';
comment on column HEE_USER.UID is '用户ID';
comment on column HEE_USER.UNAME is '用户名称';
comment on column HEE_USER.UPASSWORD is '用户密码';
*/
INSERT INTO HEE.HEE_USER (UID, UNAME, UPASSWORD) VALUES ('wanghy', '王华月', '45E6571459DA3CBC8723BC0858636589');

create table HEE_MENU (
    MID integer,
    MPID integer,
    MNAME varchar(32),
    MACTION varchar(100),
    MSORT integer,
    primary key (MID)
);
/*
comment on table HEE_MENU is '菜单表';
comment on column HEE_MENU.MID is '菜单ID';
comment on column HEE_MENU.MNAME is '菜单名称';
comment on column HEE_MENU.MACTION is '菜单Action';
comment on column HEE_MENU.MSORT is '菜单排序';
*/
INSERT INTO HEE.HEE_MENU (MID, MPID, MNAME, MACTION, MSORT) VALUES (1, 0, '基本资料', NULL, 1);
INSERT INTO HEE.HEE_MENU (MID, MPID, MNAME, MACTION, MSORT) VALUES (2, 1, '商品信息', 'me.whily.hee.view.basicData.ProInfoJPanel', 1);
INSERT INTO HEE.HEE_MENU (MID, MPID, MNAME, MACTION, MSORT) VALUES (3, 1, '职员信息', NULL, 2);
INSERT INTO HEE.HEE_MENU (MID, MPID, MNAME, MACTION, MSORT) VALUES (4, 1, '单位信息', NULL, 3);
INSERT INTO HEE.HEE_MENU (MID, MPID, MNAME, MACTION, MSORT) VALUES (5, 1, '仓库信息', NULL, 4);
INSERT INTO HEE.HEE_MENU (MID, MPID, MNAME, MACTION, MSORT) VALUES (6, 1, '结算账户', NULL, 5);

create table HEE_USER_MENU (
    UID varchar(32),
    MID integer,
    primary key (UID, MID)
);
/*
comment on table HEE_USER_MENU is '用户菜单表';
comment on column HEE_USER.UID is '用户ID';
comment on column HEE_MENU.MID is '菜单ID';
*/
INSERT INTO HEE.HEE_USER_MENU (UID, MID) VALUES ('wanghy', 1);
INSERT INTO HEE.HEE_USER_MENU (UID, MID) VALUES ('wanghy', 2);
INSERT INTO HEE.HEE_USER_MENU (UID, MID) VALUES ('wanghy', 3);
INSERT INTO HEE.HEE_USER_MENU (UID, MID) VALUES ('wanghy', 4);
INSERT INTO HEE.HEE_USER_MENU (UID, MID) VALUES ('wanghy', 5);
INSERT INTO HEE.HEE_USER_MENU (UID, MID) VALUES ('wanghy', 6);

create table HEE_PROTYPE (
    PTID integer,
    PTPID integer,
    PTNAME varchar(32),
    PTSORT integer,
    primary key (PTID)
);
/*
comment on table HEE_PROTYPE is '商品类表';
comment on column HEE_PROTYPE.PTID is '商品类ID';
comment on column HEE_PROTYPE.PTPID is '商品类父ID';
comment on column HEE_PROTYPE.PTNAME is '商品类名称';
comment on column HEE_PROTYPE.PTSORT is '商品类排序';
*/
INSERT INTO HEE.HEE_PROTYPE (PTID, PTPID, PTNAME, PTSORT) VALUES (1, 0, '全部商品', 1);

create table HEE_PRODUCT (
    PROID varchar(32),
    PRONAME varchar(32),
    PROBARCODE varchar(32),
    PROSPEC varchar(32),
    PROSRRC varchar(32),
    PROORIGIN varchar(32),
    PROCOMPANY varchar(100),
    PRORETAIL decimal(10, 2),
    PROMINPRICE decimal(10, 2),
    PROPREPRICE1 decimal(10, 2),
    PROPREPRICE2 decimal(10, 2),
    PROREMARK decimal(10, 2),
    PTID integer,
    primary key (PROID)
);
/*
comment on table HEE.HEE_PRODUCT is '商品表';
comment on column HEE_PRODUCT.PROID is '商品编号';
comment on column HEE_PRODUCT.PRONAME is '商品名称';
comment on column HEE_PRODUCT.PROBARCODE is '商品条码';
comment on column HEE_PRODUCT.PROSPEC is '商品规格';
comment on column HEE_PRODUCT.PROSRRC is '商品型号';
comment on column HEE_PRODUCT.PROORIGIN is '商品产地';
comment on column HEE_PRODUCT.PROCOMPANY is '商品单位';
comment on column HEE_PRODUCT.PRORETAIL is '商品零售价';
comment on column HEE_PRODUCT.PROMINPRICE is '商品最低售价';
comment on column HEE_PRODUCT.PROPREPRICE1 is '商品预设售价1';
comment on column HEE_PRODUCT.PROPREPRICE2 is '商品预设售价2';
comment on column HEE_PRODUCT.PROREMARK is '商品备注';
*/
select * from HEE_USER;

select * from HEE.HEE_MENU order by mpid, msort;

select * from HEE.HEE_USER_MENU;

select * from HEE.HEE_PROTYPE;

select * from HEE.HEE_PRODUCT;

select m.* from hee_menu m left join hee_user_menu um on m.mid = um.mid 
where um.uid='wanghy' order by m.mpid, m.msort
